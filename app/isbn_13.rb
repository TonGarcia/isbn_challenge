# increasing string processing performance
# frozen_string_literal: true

# ISBN13 Barcode number processing
class ISBN13
  # outside attrs
  attr_accessor :barcode, :complete_barcode, :check_digit

  def initialize(barcode)
    self.barcode = barcode
    setup
  end

  private

  def setup
    base = 10
    mod = multiply_digits % base
    self.check_digit = base - mod
    self.complete_barcode = "#{barcode}#{check_digit}"
  end

  def multiply_digits
    multiplier = 1
    processed_digits = 0
    barcode.each_char do |char|
      code = char.to_i
      processed_digits += code * multiplier
      multiplier = multiplier == 1 ? 3 : 1
    end

    processed_digits
  end
end
