# frozen_string_literal: true

require_relative '../app/isbn_13'

describe ISBN13 do
  let(:given_barcode) { '978014300723' }
  let(:expected_result) { '9780143007234' }
  let(:isbn_sample) { ISBN13.new given_barcode }

  describe 'sample given number' do
    it 'returns the expected new bar number' do
      expect(isbn_sample.complete_barcode == expected_result).to be_truthy
    end
  end
end
