[![pipeline status](https://gitlab.com/TonGarcia/isbn_challenge/badges/master/pipeline.svg)](https://gitlab.com/TonGarcia/isbn_challenge/-/commits/master)
[![coverage report](https://gitlab.com/TonGarcia/isbn_challenge/badges/master/coverage.svg)](https://gitlab.com/TonGarcia/isbn_challenge/-/commits/master)

- Tell us what creates good easy to maintain code and when are the main things that contribute to poor code:
    - Good & easy to maintain:  keep the framework convention, pay attation about DRY (do not repeat yourself), create specific reponsabilities
    - Poor code: a lot of logic and a lot of conditions in a same method that are not allowed to be reused and hard to check the integrity

- Write reliable and best practice code in ruby to calculate the check digit of given ISBN13 barcode (string) and put it into a GIT repo for us to look at.

The algorithm is:

1. Take each digit, from left to right and multiply them alternatively by 1 and 3
2. Sum those numbers
3. Take mod 10 of the summed figure
4. Subtract 10 and if the end number is 10, make it 0

Example for 978014300723:

=> (9×1) + (7×3) + (8×1) + (0×3) + (1×1) + (4×3) + (3×1) + (0×3) + (0×1) + (7×3) + (2×1) + (3×3)
=> 86
=> 86 mod 10 = 6
=> 10 - 6 = 4

Therefore the complete ISBN is: 9780143007234

# Run it Test

```shell
    $ bundle exec rspec spec
```